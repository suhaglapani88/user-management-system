import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';
import {SecurityService} from '../../security/service/security.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  @Input() userData;

  constructor(public router: Router, public securityService: SecurityService) {
  }

  logout() {
    localStorage.clear();
    sessionStorage.clear();
    if (this.securityService.userData.role === 3) {
      this.router.navigate(['/vyakarlogin']);
    } else {
      this.router.navigate(['/login']);
    }
  }
}
