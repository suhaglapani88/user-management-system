import {Component, OnInit} from '@angular/core';
import {SecurityService} from './security/service/security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  token = sessionStorage.getItem('usertoken') || localStorage.getItem('usertoken');

  constructor(public securityService: SecurityService) {
    if (this.token) {
      this.getUser();
    }
  }

  ngOnInit() {
    if (this.token) {
      this.getUser();
    }
  }

  getUser() {
    this.securityService.getUser().subscribe(o => {
      this.securityService.setUserDataOnReload(o);
    });
  }
}
