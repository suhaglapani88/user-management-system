import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared.module';
import { RouterModule } from '@angular/router';
import { WidgetsModule } from '../../widgets/widgets.module';

@NgModule({
  declarations:  [
    SharedModule,
    RouterModule,
    WidgetsModule
  ],
  imports: [
    CommonModule
  ]
})
export class ClientUserModule { }
