import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../../../security/service/security.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-create-user-client',
  templateUrl: './create-user-client.component.html',
  styleUrls: ['./create-user-client.component.css']
})
export class CreateUserClientComponent implements OnInit {

  user = {
    ClientId: 0,
    firstName: '',
    lastName: '',
    email: '',
    role: 2
  };

  clients = [];
  selectedClient;
  isContinue = false;
  userRole;

  constructor(private securityService: SecurityService,
              public notifier: NotifierService) {
  }
  ngOnInit() {

    this.userRole = sessionStorage.getItem('role') || localStorage.getItem('role');
    this.getAllClients();
    if (this.userRole === '1') {
      this.isContinue = true;
    } else {
      this.user.ClientId = this.securityService.userData.ClientId;
      this.isContinue = false;
    }
  }

  onAdd(clientId) {
    this.userRole === '1' ?  this.user.ClientId = clientId : null ;
    this.securityService.createUser(this.user).subscribe((o: any) => {
        this.userRole === '3' ?  this.isContinue = false : null ;
        if (o.success) {
          this.notifier.notify('success', 'New Client User registered.Please verify from mail');
        }
      },
      (error) => {
        this.userRole === '3' ?  this.isContinue = false : null ;
        this.notifier.notify('error', error.error.message);
      });
  }


  changeClient(id) {
    this.selectedClient = this.clients.find((o) => o.id === id);
  }

  onAddContinue() {
    this.isContinue = true;
  }

  getAllClients() {
    this.securityService.getAllClient().subscribe((o: any) => {
      this.clients = o.clients;
    });
  }

}
