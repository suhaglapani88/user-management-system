import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUserClientComponent } from './create-user-client.component';

describe('CreateUserClientComponent', () => {
  let component: CreateUserClientComponent;
  let fixture: ComponentFixture<CreateUserClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUserClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUserClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
