import {Component, OnInit} from '@angular/core';
import {VyakarUser} from '../../../models/vyakaruser';
import {SecurityService} from '../../../security/service/security.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-create-new-vyakar',
  templateUrl: './create-new-vyakar.component.html',
  styleUrls: ['./create-new-vyakar.component.css']
})
export class CreateNewVyakarComponent implements OnInit {

  user: VyakarUser = {
    firstName: '',
    lastName: '',
    email: ''
  };
  private readonly notifier: NotifierService;

  constructor(private securityService: SecurityService,
              notifier: NotifierService) {
    this.notifier = notifier;
  }

  ngOnInit() {
  }

  onAdd() {
    this.securityService.createNewVyakar(this.user).subscribe((o: any) => {
        if (o.success) {
          this.notifier.notify('success', 'New vyakar registered.Please verify from mail');
        }
      },
      (error) => {
        this.notifier.notify('error', error.error.message);
      }
    );
  }
}
