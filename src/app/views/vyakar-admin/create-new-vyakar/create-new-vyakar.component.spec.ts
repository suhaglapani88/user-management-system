import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNewVyakarComponent } from './create-new-vyakar.component';

describe('CreateNewVyakarComponent', () => {
  let component: CreateNewVyakarComponent;
  let fixture: ComponentFixture<CreateNewVyakarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateNewVyakarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewVyakarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
