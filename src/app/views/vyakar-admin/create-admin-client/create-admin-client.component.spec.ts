import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateAdminClientComponent} from './create-admin-client.component';

describe('CreateAdminClientComponent', () => {
  let component: CreateAdminClientComponent;
  let fixture: ComponentFixture<CreateAdminClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateAdminClientComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAdminClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
