import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../../../security/service/security.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-create-admin-client',
  templateUrl: './create-admin-client.component.html',
  styleUrls: ['./create-admin-client.component.css']
})
export class CreateAdminClientComponent implements OnInit {

  createSuccess = false;
  isError = false;
  user = {
    ClientId: 0,
    firstName: '',
    lastName: '',
    email: '',
    role: 1
  };

  clients = [];
  selectedClient;
  isContinue = false;
  userRole;

  private readonly notifier: NotifierService;

  constructor(private securityService: SecurityService,
              public notifierService: NotifierService,) {
    this.notifier = notifierService;
  }

  ngOnInit() {

    this.userRole = sessionStorage.getItem('role') || localStorage.getItem('role');
    this.getAllClients();
    if (this.userRole === '1') {
      this.isContinue = true;
    } else {
      this.isContinue = false;
    }

  }

  onAdd(clientId) {
    this.userRole === '1' ?  this.user.ClientId = clientId : null ;
    this.securityService.createUser(this.user).subscribe((o: any) => {
        this.userRole === '3' ?  this.isContinue = false : null ;
        if (o.success) {
          this.notifier.notify('success', 'New Client Admin registered.Please verify from mail');
        }
      },
      (error) => {
        this.userRole === '3' ?  this.isContinue = false : null ;
        this.notifier.notify('error', error.error.message);
      });
  }

  getAllClients() {
    this.securityService.getAllClient().subscribe((o: any) => {
      this.clients = o.clients;
    });
  }

  changeClient(id) {
    this.selectedClient = this.clients.find((o) => o.id === id);
  }

  onAddContinue() {
    this.isContinue = true;
  }
}
