import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared.module';
import {RouterModule} from '@angular/router';
import {WidgetsModule} from '../../widgets/widgets.module';
import {CreateNewVyakarComponent} from './create-new-vyakar/create-new-vyakar.component';
import {CreateAdminClientComponent} from './create-admin-client/create-admin-client.component';
import {CreateUserClientComponent} from './create-user-client/create-user-client.component';
import {AdminDialogEditUserDialog, DeleteDialog, ResetPasswordDialog, VyakarListComponent} from './vyakar-list/vyakar-list.component';
import {
  AdminClientListComponent,
  AdminDialogEditAdmiDialog, DeleteDialogAdmin,
  ResetPasswordDialogAdmin
} from './admin-client-list/admin-client-list.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule, MatPaginatorModule, MatSortModule} from '@angular/material';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {MatIconModule} from '@angular/material/icon';
import {
  DeleteDialogUser,
  DialogEditUserDialog,
  ResetPasswordDialogUser,
  UserClientListComponent
} from './user-client-list/user-client-list.component';
import {NotifierModule, NotifierOptions} from 'angular-notifier';

const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'right',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 12,
      gap: 10
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 2000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};

@NgModule({
  declarations: [
    CreateNewVyakarComponent,
    CreateAdminClientComponent,
    CreateUserClientComponent,
    VyakarListComponent,
    AdminClientListComponent,
    AdminDialogEditAdmiDialog,
    AdminDialogEditUserDialog,
    DeleteDialog,
    ResetPasswordDialog,
    ResetPasswordDialogAdmin,
    DeleteDialogAdmin,
    ResetPasswordDialogUser,
    DeleteDialogUser,
    UserClientListComponent,
    DialogEditUserDialog
  ],
  exports: [
    SharedModule,
    RouterModule,
    WidgetsModule,
    CreateNewVyakarComponent,
    CreateAdminClientComponent,
    CreateUserClientComponent,
    VyakarListComponent,
    AdminClientListComponent,
    AdminDialogEditAdmiDialog,
    AdminDialogEditUserDialog,
    DeleteDialog,
    ResetPasswordDialog,
    ResetPasswordDialogAdmin,
    DeleteDialogAdmin,
    ResetPasswordDialogUser,
    DeleteDialogUser,
    UserClientListComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    RouterModule,
    WidgetsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCardModule,
    MatButtonModule,
    FormsModule,
    MatInputModule,
    FlexLayoutModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    AngularFontAwesomeModule,
    MatIconModule,
    NotifierModule.withConfig(customNotifierOptions)
  ],
  entryComponents: [
    AdminDialogEditAdmiDialog,
    DeleteDialog,
    ResetPasswordDialog,
    ResetPasswordDialogAdmin,
    DeleteDialogAdmin,
    ResetPasswordDialogUser,
    DeleteDialogUser,
    AdminDialogEditUserDialog,
    DialogEditUserDialog
  ],
})
export class VyakarAdminModule {
}
