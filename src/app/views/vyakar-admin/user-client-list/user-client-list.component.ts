import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

import {SecurityService} from '../../../security/service/security.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-admin-client-list',
  templateUrl: './user-client-list.component.html',
  styleUrls: ['./user-client-list.component.css']
})
export class UserClientListComponent implements OnInit {
  displayedColumns: string[] = ['ClientId', 'id', 'firstName', 'lastName', 'email', 'delete', 'edit', 'forgotpassword'];
  dataSource;

  userRole = sessionStorage.getItem('role') || localStorage.getItem('role');
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public securityService: SecurityService,
    public dialog: MatDialog) {
  }

  ngOnInit() {
    this.userRole = sessionStorage.getItem('role') || localStorage.getItem('role');
    if (this.userRole === '3' && localStorage.getItem('usertoken')) {
      this.getAllClientUser();
    } else if (localStorage.getItem('usertoken')) {
      this.getAllClientUserById();
    }
  }

  openDialog(data): void {
    const dialogRef = this.dialog.open(DialogEditUserDialog, {
      width: '400px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (this.userRole  === '3' && localStorage.getItem('usertoken')) {
        this.getAllClientUser();
      } else if (localStorage.getItem('usertoken')) {
        this.getAllClientUserById();
      }
    });
  }

  getAllClientUser() {
    this.securityService.getAllClientUser().subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  getAllClientUserById() {
    this.securityService.getAllClientListById(1).subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data.clients);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onDelete(data) {
    const dialogRef = this.dialog.open(DeleteDialogUser, {
      width: '400px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (this.userRole === '3' && localStorage.getItem('usertoken')) {
        this.getAllClientUser();
      } else if (localStorage.getItem('usertoken')) {
        this.getAllClientUserById();
      }
    });
  }

  onResetPassword(data) {
    this.dialog.open(ResetPasswordDialogUser, {
      width: '400px',
      data: data
    });
  }
}


@Component({
  selector: 'app-dialog-edit-user-client-dialog',
  templateUrl: 'app-dialog-edit-user-client-dialog.html.html',
  styles: [`.mat-form-field {
    width: 100%
  }`]
})
export class DialogEditUserDialog {

  user = {
    firstName: '',
    lastName: '',
    email: '',
    id: 0,
    role: 1,
  };
  roles = [{id: 1, name: 'Client Admin'}, {id: 2, name: 'Client User'}];

  constructor(
    public notifier: NotifierService,
    public securityService: SecurityService,
    public dialogRef: MatDialogRef<DialogEditUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.user = this.data;
  }

  onAdd() {
    this.securityService.updateUser(this.user.id, this.user).subscribe(o => {
      this.notifier.notify('success', 'Client User has been updated successfully');
    });
    this.dialogRef.close();
  }

}


@Component({
  selector: 'delete-confirmation-dialog.html',
  templateUrl: 'delete-confirmation-dialog.html',
})
export class DeleteDialogUser {
  private readonly notifier: NotifierService;

  constructor(
    public securityService: SecurityService,
    public notifierService: NotifierService,
    public dialogRef: MatDialogRef<DeleteDialogUser>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.notifier = notifierService;
  }

  onCancel() {
    this.dialogRef.close();
  }

  onDelete() {
    this.securityService.deleteUser(this.data.id).subscribe(o => {
      this.notifier.notify('success', 'Client User has been deleted successfully');
      this.dialogRef.close();
    });
  }
}

@Component({
  selector: 'reset-password-confirmation-dialog',
  templateUrl: 'reset-password-confirmation-dialog.html',
})
export class ResetPasswordDialogUser {

  constructor(
    public notifier: NotifierService,
    public securityService: SecurityService,
    public dialogRef: MatDialogRef<ResetPasswordDialogUser>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  onCancel() {
    this.dialogRef.close();
  }

  onResetPassword() {
    this.securityService.createLinkforNewPasswordUser(
      {
        email: this.data.email,
        ClientId: this.data.ClientId,
        role: 2
      }).subscribe(o => {
      this.notifier.notify('success', 'Reset Password link has been sent successfully');
      this.dialogRef.close();
    });
  }


}

