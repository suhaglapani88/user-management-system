import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserClientListComponent } from './user-client-list.component';

describe('UserClientListComponent', () => {
  let component: UserClientListComponent;
  let fixture: ComponentFixture<UserClientListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserClientListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserClientListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
