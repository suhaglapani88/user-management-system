import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VyakarListComponent } from './vyakar-list.component';

describe('VyakarListComponent', () => {
  let component: VyakarListComponent;
  let fixture: ComponentFixture<VyakarListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VyakarListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VyakarListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
