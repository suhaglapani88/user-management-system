import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SecurityService} from '../../../security/service/security.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-vyakar-list',
  templateUrl: './vyakar-list.component.html',
  styleUrls: ['./vyakar-list.component.css']
})
export class VyakarListComponent implements OnInit {
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'email', 'delete', 'edit', 'forgotpassword' ];
  dataSource;


 @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog,
              public securityService: SecurityService) {}

  ngOnInit() {
    this.getAllVyakar();
  }

  openDialog(data): void {
    const dialogRef = this.dialog.open(AdminDialogEditUserDialog, {
      width: '400px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  getAllVyakar() {
    this.securityService.getAllVyakar().subscribe( (data: any) => {
      this.dataSource = new MatTableDataSource(data.vyakar);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onDelete(data) {
   const dialogRef = this.dialog.open(DeleteDialog, {
      width: '400px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllVyakar();
    });

  }

  onResetPassword(data) {
    this.dialog.open(ResetPasswordDialog, {
      width: '400px',
      data: data
    });
  }
}


@Component({
  selector: 'app-dialog-user-admin-client-dialog',
  templateUrl: 'app-dialog-edit-user-client-dialog.html',
  styles: [`.mat-form-field {width: 100% }`]
})
export class AdminDialogEditUserDialog {

  user = {
    firstName: '',
    lastName: '',
    email: '',
    id: 0
  }
  private readonly notifier: NotifierService;
  constructor(
    public notifierService: NotifierService,
    public securityService: SecurityService,
    public dialogRef: MatDialogRef<AdminDialogEditUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.user = this.data;
    this.notifier = notifierService;
  }

  onAdd() {
    this.securityService.updateVyakar(this.user).subscribe( (o: any) => {
      this.notifier.notify( 'success', 'Vyakar user has been updated successfully' );
      this.dialogRef.close();
    });
  }
}

@Component({
  selector: 'delete-confirmation-dialog.html',
  templateUrl: 'delete-confirmation-dialog.html',
})
export class DeleteDialog {


  private readonly notifier: NotifierService;

  constructor(
    public notifierService: NotifierService,
    public securityService: SecurityService,
    public dialogRef: MatDialogRef<DeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.notifier = notifierService;
  }

  onCancel() {
    this.dialogRef.close();
  }

  onDelete() {
    this.securityService.deleteVyakar(this.data.id).subscribe( o => {
      this.notifier.notify( 'success', 'Vyakar user has been deleted successfully' );
      this.dialogRef.close();
    });
  }
}

@Component({
  selector: 'reset-password-confirmation-dialog',
  templateUrl: 'reset-password-confirmation-dialog.html',
})
export class ResetPasswordDialog {

  private readonly notifier: NotifierService;

  constructor(
    public notifierService: NotifierService,
    public securityService: SecurityService,
    public dialogRef: MatDialogRef<ResetPasswordDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.notifier = notifierService;
  }

  onCancel() {
    this.dialogRef.close();
  }

  onResetPassword() {
    this.securityService.createLinkforNewPassword(this.data.email).subscribe( o => {
      this.notifier.notify( 'success', 'Reset Password link has been sent successfully' );
      this.dialogRef.close();
    });
  }
}

