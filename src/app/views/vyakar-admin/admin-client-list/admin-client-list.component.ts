import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SecurityService} from '../../../security/service/security.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-admin-client-list',
  templateUrl: './admin-client-list.component.html',
  styleUrls: ['./admin-client-list.component.css']
})
export class AdminClientListComponent implements OnInit {
  displayedColumns: string[] = ['ClientId', 'id', 'firstName', 'lastName', 'email', 'delete', 'edit', 'forgotpassword'];
  dataSource;
  userRole

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public securityService: SecurityService,
    public dialog: MatDialog) {
  }

  ngOnInit() {

    this.userRole = sessionStorage.getItem('role') || localStorage.getItem('role');
    if ( this.userRole  === '3' && localStorage.getItem('usertoken')) {
      this.getAllAdmin();
    } else if (localStorage.getItem('usertoken')) {
      this.getAllgetAllAdminsListById();
    }
  }

  openDialog(data): void {
    const dialogRef = this.dialog.open(AdminDialogEditAdmiDialog, {
      width: '400px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (this.userRole  === '3' && localStorage.getItem('usertoken')) {
        this.getAllAdmin();
      } else if (localStorage.getItem('usertoken')) {
        this.getAllgetAllAdminsListById();
      }
    });
  }

  getAllAdmin() {
    this.securityService.getAllAdmin().subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data.admins);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onDelete(data) {
    const dialogRef = this.dialog.open(DeleteDialogAdmin, {
      width: '400px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (this.userRole  === '3' && localStorage.getItem('usertoken')) {
        this.getAllAdmin();
      } else if (localStorage.getItem('usertoken')) {
        this.getAllgetAllAdminsListById();
      }
    });
  }

  onResetPassword(data) {
    this.dialog.open(ResetPasswordDialogAdmin, {
      width: '400px',
      data: data
    });
  }

  getAllgetAllAdminsListById() {
    this.securityService.getAllgetAllAdminsListById(1).subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data.admins);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}


@Component({
  selector: 'app-dialog-edit-admin-client-dialog',
  templateUrl: 'app-dialog-edit-admin-client-dialog.html.html',
  styles: [`.mat-form-field {
    width: 100%
  }`]
})
export class AdminDialogEditAdmiDialog {

  user = {
    firstName: '',
    lastName: '',
    email: '',
    id: 0,
    role: 1,
  };
  private readonly notifier: NotifierService;
  roles = [{id: 1, name: 'Client Admin'}, {id: 2, name: 'Client User'}];

  constructor(
    public notifierService: NotifierService,
    public securityService: SecurityService,
    public dialogRef: MatDialogRef<AdminDialogEditAdmiDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.user = this.data;
    this.notifier = notifierService;
  }

  onAdd() {
    this.securityService.updateUser(this.user.id, this.user).subscribe(o => {
      this.notifier.notify('success', 'Client Admin has been updated successfully');
    });
    this.dialogRef.close();
  }

}


@Component({
  selector: 'delete-confirmation-dialog.html',
  templateUrl: 'delete-confirmation-dialog.html',
})
export class DeleteDialogAdmin {

  private readonly notifier: NotifierService;

  constructor(
    public notifierService: NotifierService,
    public securityService: SecurityService,
    public dialogRef: MatDialogRef<DeleteDialogAdmin>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.notifier = notifierService;
  }

  onCancel() {
    this.dialogRef.close();
  }

  onDelete() {
    this.securityService.deleteUser(this.data.id).subscribe(o => {
      this.notifier.notify('success', 'Client Admin has been deleted successfully');
      this.dialogRef.close();
    });
  }
}

@Component({
  selector: 'reset-password-confirmation-dialog',
  templateUrl: 'reset-password-confirmation-dialog.html',
})
export class ResetPasswordDialogAdmin {
  private readonly notifier: NotifierService;

  constructor(
    public notifierService: NotifierService,
    public securityService: SecurityService,
    public dialogRef: MatDialogRef<ResetPasswordDialogAdmin>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.notifier = notifierService;
  }

  onCancel() {
    this.dialogRef.close();
  }

  onResetPassword() {
    this.securityService.createLinkforNewPasswordUser(
      {
        email: this.data.email,
        ClientId: this.data.ClientId,
        role: 1
      }).subscribe(o => {
      this.notifier.notify('success', 'Reset Password link has been sent successfully');
      this.dialogRef.close();
    });
  }
}

