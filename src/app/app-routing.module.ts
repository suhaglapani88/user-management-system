import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagenotfoundComponent } from './widgets/pagenotfound/pagenotfound.component';
import { LoginComponent } from './security/views/login/login.component';
import { ChangePasswordComponent } from './security/views/change-password/change-password.component';
import { ForgotPasswordComponent } from './security/views/forgot-password/forgot-password.component';
import { CreateNewVyakarComponent } from './views/vyakar-admin/create-new-vyakar/create-new-vyakar.component';
import { CreateAdminClientComponent } from './views/vyakar-admin/create-admin-client/create-admin-client.component';
import { CreateUserClientComponent } from './views/vyakar-admin/create-user-client/create-user-client.component';
import { AdminClientListComponent } from './views/vyakar-admin/admin-client-list/admin-client-list.component';
import { VyakarListComponent } from './views/vyakar-admin/vyakar-list/vyakar-list.component';
import { AuthGuard } from './security/service/auth.guard';
import { UserClientListComponent } from './views/vyakar-admin/user-client-list/user-client-list.component';
import {ImpersonateComponent} from './security/views/impersonate/impersonate.component';

const routes: Routes = [
  {
    path: 'changepassword',
    component: ChangePasswordComponent,
    canActivate: [AuthGuard],
  },
  { path: 'createNewPassword/:role/:token',
    component: ForgotPasswordComponent
  },
  { path: 'creatvyakar',
    component: CreateNewVyakarComponent,
    canActivate: [AuthGuard],
  },
  { path: 'creatclientadmin',
    component: CreateAdminClientComponent,
    canActivate: [AuthGuard],
  },
  { path: 'creatclientuser',
    component: CreateUserClientComponent,
    canActivate: [AuthGuard],
  },
  { path: 'clientadminlist',
    component: AdminClientListComponent,
    canActivate: [AuthGuard],
  },
  { path: 'vyakarlist',
    component: VyakarListComponent,
    canActivate: [AuthGuard],
  },
  { path: 'clientuserlist',
    component: UserClientListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'vyakarlogin',
    component: LoginComponent,
  },
  {
    path: 'impersonate',
    component: ImpersonateComponent,
    canActivate: [AuthGuard],
  },
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
