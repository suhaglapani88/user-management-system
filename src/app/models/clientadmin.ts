export class Clientadmin {
  clientid?: string;
  firstName: string;
  lastName: string;
  email: string;
  underuser?: Array<string>;
  role?: number;
}
