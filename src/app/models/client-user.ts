export class Clientuser {
  clientid?: string;
  firstName: string;
  lastName: string;
  email: string;
  admins: Array<string>;
}
