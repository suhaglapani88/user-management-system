import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ForgotPasswordComponent} from './views/forgot-password/forgot-password.component';
import {LoginComponent} from './views/login/login.component';
import {ChangePasswordComponent} from './views/change-password/change-password.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {ValidatorsModule} from '../validators/validators.module';
import { ImpersonateComponent } from './views/impersonate/impersonate.component';

@NgModule({
  declarations: [
    ForgotPasswordComponent,
    LoginComponent,
    ChangePasswordComponent,
    ImpersonateComponent,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCardModule,
    MatButtonModule,
    FormsModule,
    MatInputModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    ValidatorsModule
  ],
  exports: [
    ForgotPasswordComponent,
    LoginComponent,
    ChangePasswordComponent
  ]
})
export class SecurityModule {
}
