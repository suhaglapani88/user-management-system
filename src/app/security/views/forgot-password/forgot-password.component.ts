import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../../service/security.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  forgotPassword: string;
  isSetForgotPassword = false;
  userdata;
  role;
  token;

  constructor(private securityService: SecurityService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.role = this.route.snapshot.paramMap.get('role');
    this.token = this.route.snapshot.paramMap.get('token');
  }

  onForgotPasswordPassword() {
    if (this.role === 'vyakar' && this.token) {
      this.securityService.createNewPasswordVyakar({newPassword: this.forgotPassword, token: this.token}).subscribe(o => {
        this.isSetForgotPassword = true;
        this.userdata = o;
      });
    } else {
      this.securityService.createNewPassword({newPassword: this.forgotPassword, token: this.token}).subscribe(o => {
        this.isSetForgotPassword = true;
        this.userdata = o;
      });
    }
  }
}


