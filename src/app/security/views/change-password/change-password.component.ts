import {Component} from '@angular/core';
import {SecurityService} from '../../service/security.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent {

  passwordData = {
    currentPassword: '',
    newPassword: '',
    confirmNewPassword: '',
  };
  private readonly notifier: NotifierService;

  constructor(private securityService: SecurityService,
              public notifierService: NotifierService,) {
    this.notifier = notifierService;
  }

  onchangePassword() {
    this.securityService.changePasswoed({
      oldPassword: this.passwordData.currentPassword,
      newPassword: this.passwordData.newPassword,
    }, this.securityService.userData.id).subscribe((o: any) => {
      if (o.message) {
        this.notifier.notify('error', 'Old password is incorrect.');
      } else {
        this.notifier.notify('success', 'Password has been changed successfully.');
      }
    });
  }
}
