import {Component, OnInit} from '@angular/core';
import {Login} from '../../models/login';
import {SecurityService} from '../../service/security.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public isLoggedIn = false;
  invalidLogin = false;
  loginData: Login = {
    ClientId: '',
    email: '',
    password: '',
  };

  constructor(private securityService: SecurityService,
              public router: Router) {
  }

  ngOnInit() {
  }

  onLogin() {
    if (this.router.url === '/login') {
      this.securityService.isLogin(
        {
          'ClientId': this.loginData.ClientId,
          'email': this.loginData.email,
          'password': this.loginData.password
        })
        .subscribe((data: any) => {
            this.isLoggedIn = true;
            this.securityService.setUserData(data);
            this.securityService.getUser();
          },
          (e) => {
            this.invalidLogin = true;
          });
    } else if (this.router.url === '/vyakarlogin') {
      this.securityService.isVyakarLogin(
        {
          'email': this.loginData.email,
          'password': this.loginData.password
        }
      ).subscribe((data: any) => {
          this.isLoggedIn = true;
          this.securityService.setUserData(data);
          this.securityService.getVyakarProfile();
        },
        (e) => {
          this.invalidLogin = true;
        });
    }
  }
}
