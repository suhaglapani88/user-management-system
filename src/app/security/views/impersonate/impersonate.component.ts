import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../../service/security.service';

@Component({
  selector: 'app-impersonate',
  templateUrl: './impersonate.component.html',
  styleUrls: ['./impersonate.component.css']
})
export class ImpersonateComponent implements OnInit {

  ClientId;
  clients = [];
  users = []
  userData;

  constructor(private securityService: SecurityService,) { }

  ngOnInit() {
    this.getAllClients();
  }

  getAllClients() {
    this.securityService.getAllClient().subscribe((o: any) => {
      this.clients = o.clients;
    });
  }

  changeClient(data) {
    this.securityService.getAllUserOfClient(data).subscribe((o: any) => {
      this.users = o.users;
    });
  }

  onLogin() {
    this.securityService.loginAsImpersonate(this.userData).subscribe( (o: any) => {
      let w;
      if (o.user.role === 1) {
       w = window.open('http://localhost:4200/clientadminlist');
      } else {
        w =  window.open('http://localhost:4200/changepassword');
      }
      w.addEventListener('load', () => {
        w.sessionStorage.setItem('role', o.user.role);
        w.sessionStorage.setItem('usertoken', o.token);
        w.sessionStorage.setItem('tempUser', JSON.stringify(o.user));
        w.location.reload();
      });
    });
  }
}
