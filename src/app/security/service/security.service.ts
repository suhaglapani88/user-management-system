import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Login } from '../models/login';
import { environment } from '../../../environments/environment';

const authUrl =  environment.baseApiUrl + 'auth/';
const url =  environment.baseApiUrl + 'users/';
const vyakarAuth = environment.baseApiUrl + 'secret/vyakarAdmin/'

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  userToken;
  userData;
  headers;

  userRole;
  token;
  constructor(private router: Router,
              private httpclient: HttpClient) {
    this.userRole = sessionStorage.getItem('role') || localStorage.getItem('role');
    this.token =  sessionStorage.getItem('usertoken') || localStorage.getItem('usertoken');
    this.headers = new HttpHeaders({'Authorization': `Bearer ${this.token}`});

  }

  isLogin(loginData: Login) {
    return this.httpclient.post(authUrl + 'login', loginData);
  }

  isVyakarLogin(loginData) {
    return this.httpclient.post(authUrl + 'vyakarLogin', loginData);
  }

  setUserData(data) {
    this.userToken = data.token;
    this.userData = data.user;
    localStorage.setItem('role', data.user.role);
    localStorage.setItem('usertoken', this.userToken);
    this.headers = new HttpHeaders({'Authorization': `Bearer ${localStorage.getItem('usertoken')}`});

    if (this.userData.role === 3) {
      this.router.navigate(['/vyakarlist']);
    } else if (this.userData.role === 1) {
      this.router.navigate(['/clientadminlist']);
    } else if (this.userData.role === 2) {
      this.router.navigate(['/changepassword']);
    }
  }

  setUserDataOnReload(data) {
    this.userData =  data;
  }

  // get user data of client
  getUser() {
    if ( + this.userRole === 3) {
      return this.httpclient.get(vyakarAuth + 'profile', {headers: this.headers});
    } else {
      return this.httpclient.get(url + 'profile' , { headers: this.headers });
    }
  }

  getVyakarProfile() {
    return this.httpclient.get(vyakarAuth + 'profile' , { headers: this.headers });
  }

  /*// get user data of client
  getVyakarUser() {
    return this.httpclient.get(url + 'profile' , { headers });
  }*/

  // change password client admin and client user
  changePasswoed(passwordData, id) {
    if ( this.userData.role === 3 ) {
      return this.httpclient.put(vyakarAuth + 'updatePassword/' , passwordData , { headers: this.headers });
    } else {
      return this.httpclient.put(url + 'updatePassword/' + id , passwordData , { headers: this.headers });
    }
  }

  getAllVyakar() {
    return this.httpclient.get(vyakarAuth + 'getAllVyakar/'  , { headers: this.headers });
  }

  deleteVyakar(id) {
    return this.httpclient.delete(vyakarAuth + 'deleteVyakar/' + id , { headers: this.headers });
  }

  createLinkforNewPassword(email) {
    return this.httpclient.put(vyakarAuth + 'createLinkforNewPassword/', {email},{ headers: this.headers });
  }

  createLinkforNewPasswordUser(data) {
    return this.httpclient.post(authUrl + 'createLinkfornewPassword/', data,{ headers: this.headers });
  }
  /*// change password client admin and client user
  changePasswordVyakar(passwordData, id) {
    return this.httpclient.put(vyakarAuth + 'updatePassword/' , passwordData , { headers });
  }*/

  /** POST /api/secret/vyakarAdmin/createNewVyakar - create New vyakar admin */
  /*body: {
    firstName: ,
    lastName: ,
    email: ,
  },*/
  createNewVyakar(body) {
    return this.httpclient.post(vyakarAuth + 'createNewVyakar', body, {headers: this.headers});
  }

  /** PUT /api/auth/createNewPasswordVyakar - Create new password for vyakar*/
  /*body :{
    token : ,
    newPassword: ,
  }*/
  createNewPasswordVyakar(body) {
    return this.httpclient.put(authUrl + 'createNewPasswordVyakar', body);
  }

  updateVyakar(body) {
    return this.httpclient.put(vyakarAuth + 'updateVyakar', body, {headers: this.headers});
  }
  /** GET /api/secret/vyakarAdmin/getAllClient -get all clients */
  getAllClient() {
    return this.httpclient.get(vyakarAuth + 'getAllClient', {headers: this.headers});
  }

  /* body: {
      email:,
      firstName: ,
      lastName: ,
      ClientId : ,
      role :
    }, */
  /** POST /api/auth/register - Register a new user */
  createUser(body) {
    return this.httpclient.post(authUrl + 'register', body);
  }

  createNewPassword(data) {
    return this.httpclient.put(authUrl + 'createNewPassword', data);
  }

  getAllClientListById(id) {
    return this.httpclient.get(url  + id + '/getAllClients'  , { headers: this.headers });
  }

  getAllgetAllAdminsListById(id) {
    return this.httpclient.get(url  + id + '/getAllAdmins'  , { headers: this.headers });
  }

  getAllAdmin() {
    return this.httpclient.get(vyakarAuth + 'getAllClientAdmin'  , { headers: this.headers });
  }

  getAllClientUser() {
    return this.httpclient.get(vyakarAuth + 'getAllClientUser'  , { headers: this.headers });
  }

  /** PUT /api/users/:userId - Update user */
  updateUser(id, body) {
    return this.httpclient.put(url + id, body, { headers: this.headers });
  }

  /** DELETE /api/users/:userId - Delete user */
  deleteUser(id) {
    return this.httpclient.delete(url + id, { headers: this.headers });
  }

  getAllUserOfClient(clientID) {
    return this.httpclient.get(vyakarAuth + 'getAllUser/'+ clientID,  {headers: this.headers});
  }

  loginAsImpersonate(email) {
    return this.httpclient.post(vyakarAuth + 'loginAs', {email}, {headers: this.headers});
  }
}

