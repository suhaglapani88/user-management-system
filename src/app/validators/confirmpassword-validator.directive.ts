import { Directive, forwardRef, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[confirmPassword][formControlName],[confirmPassword][formControl],[confirmPassword][ngModel]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => ConfirmpasswordValidatorDirective),
    multi: true,
  }],
})
export class ConfirmpasswordValidatorDirective implements Validator {
  @Input() confirmPassword: string;

  validate(c: AbstractControl): ValidationErrors | null {
    return this.confirmPasswordCheck(this.confirmPassword)(c);
  }

  confirmPasswordCheck(testValue: string): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
      const value = control.value;
      return value === testValue ? null : {'confirmPassword': true};
    };
  }
}
