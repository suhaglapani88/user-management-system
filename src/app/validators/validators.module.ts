import { NgModule } from '@angular/core';
import { ConfirmpasswordValidatorDirective } from './confirmpassword-validator.directive';

const DIRECTIVES = [ConfirmpasswordValidatorDirective];
@NgModule({
  exports: DIRECTIVES,
  declarations: DIRECTIVES
})
export class ValidatorsModule { }
