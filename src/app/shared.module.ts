import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { SecurityModule } from './security/security.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ValidatorsModule } from './validators/validators.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    SecurityModule,
    AngularFontAwesomeModule,
    ValidatorsModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SecurityModule,
    ValidatorsModule
  ],
})
export class SharedModule { }
